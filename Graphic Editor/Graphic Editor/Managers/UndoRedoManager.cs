﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;


namespace Graphic_Editor.Managers
{
    public class UndoRedoManager
    {
        private Stack<ObservableCollection<IDrawable>> UndoList { get; set; }
        private Stack<ObservableCollection<IDrawable>> RedoList { get; set; }
        public UndoRedoManager()
        {
            UndoList = new Stack<ObservableCollection<IDrawable>>();
            RedoList = new Stack<ObservableCollection<IDrawable>>();
        }

        public void saveState(ObservableCollection<IDrawable> state)
        {
            RedoList.Clear();
            var items = new ObservableCollection<IDrawable>();
            foreach (IDrawable item in state)
            {
                items.Add(item);
            }
            UndoList.Push(items);
            Debug.WriteLine("SaveState UndoList " + UndoList.Count + " RedoLiist " + RedoList.Count);
        }
        public bool undo(ref ObservableCollection<IDrawable> state)
        {
            if(UndoList.Count > 0)
            {
                ObservableCollection<IDrawable> collection = UndoList.Pop();
                RedoList.Push(collection);
                if (UndoList.Count > 0)
                {
                    state = new ObservableCollection<IDrawable>(UndoList.First());
                    return true;
                }

                return false;
            }
            return false;
        }
        public bool redo(ref ObservableCollection<IDrawable> state)
        {
            if(RedoList.Count > 0)
            {
                ObservableCollection<IDrawable> collection = RedoList.Pop();
                UndoList.Push(collection);
                state = new ObservableCollection<IDrawable>(collection);
                return true;
            }
            return false;
        }
    }
}
