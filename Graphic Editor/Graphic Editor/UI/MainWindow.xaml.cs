﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Collections.ObjectModel;
using Graphic_Editor.Managers;
using System.Windows.Markup;
using System.Xml;
using System.Windows.Controls.Primitives;


namespace Graphic_Editor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        enum Operations
        {
            None = 1,
            AddingRectangle,
            AddingEllipse,
            AddingLine,
            AddingPolygon,
            PaintBrush,
            PaintPencil,
            Erase,
            DragCanvas,
            Fill
        }
        Brush firstColor { get; set; }
        Brush secondColor { get; set; }
        Brush fillColor { get; set; }
        double thickness;
        ToggleButton rectangleBtn;
        ToggleButton ellipseBtn;
        ToggleButton polygonBtn;
        Operations _currentOperation = Operations.None;
        public MainWindow()
        {
            InitializeComponent();
            firstColor = Brushes.Black;
            secondColor = Brushes.White;
            fillColor = Brushes.White;
            thicknessComb.SelectedIndex = 0;
            frontBtn.Background = firstColor;
            backBtn.Background = secondColor;
            CanvasPosStr = "pos: 0,0";
            CanvasSizeStr = "size: " + canvas.RenderSize;            
            DataContext = this;
            figures = new ObservableCollection<IDrawable>();
            figures.CollectionChanged += onFiguresChanged;
            undoRedoManager.saveState(figures);
            rectangleBtn = new ToggleButton() { ToolTip="Прямокутник", Width = 25, Height = 25, Background = Brushes.Transparent, Content = new Image { Source = new BitmapImage(new Uri("Images/rec.png", UriKind.Relative)) } };
            ellipseBtn = new ToggleButton() { ToolTip="Еліпс", Width = 25, Height = 25, Background = Brushes.Transparent, Content = new Image { Source = new BitmapImage(new Uri("Images/ellipse.png", UriKind.Relative)) } };
            polygonBtn = new ToggleButton() { ToolTip="Полігон", Width = 25, Height = 25, Background = Brushes.Transparent, Content = new Image { Source = new BitmapImage(new Uri("Images/polygon.png", UriKind.Relative)) } };
            rectangleBtn.Click += Rectangle_Click;
            ellipseBtn.Click += ellipseBtn_Click;
            polygonBtn.Click +=polygonBtn_Click;
            toolBox.AddButtonToStack(rectangleBtn);
            toolBox.AddButtonToStack(ellipseBtn);
            toolBox.AddButtonToStack(polygonBtn);
            cursorButton.IsChecked = true;
        }
        private void UncheckAllButtons()
        {
            rectangleBtn.IsChecked = false;
            ellipseBtn.IsChecked = false;
            polygonBtn.IsChecked = false;
            cursorButton.IsChecked = false;
            lineButton.IsChecked = false;
            fillButton.IsChecked = false;
            eraseButton.IsChecked = false;
            brushButton.IsChecked = false;
            pencilButton.IsChecked = false;
            dragButton.IsChecked = false;
            Mouse.OverrideCursor = Cursors.Arrow;
            isFirstPoint = true;
            thicknessComb.SelectedIndex = 0;
        }
        private void cursorBtnClicked(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            cursorButton.IsChecked = true;
            _currentOperation = Operations.None;
        }
        private void polygonBtn_Click(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            polygonBtn.IsChecked = true;
            toolBox.hide();
            if (_currentOperation == Operations.AddingPolygon)
            {
                _currentOperation = Operations.None;
            }
            else
            {
                _currentOperation = Operations.AddingPolygon;
            }
        }

        private void ellipseBtn_Click(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            ellipseBtn.IsChecked = true;
            toolBox.hide();
            if (_currentOperation == Operations.AddingEllipse)
            {
                _currentOperation = Operations.None;
            }
            else
            {
                _currentOperation = Operations.AddingEllipse;
            }
        }

        private void Rectangle_Click(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            rectangleBtn.IsChecked = true;
            toolBox.hide();
            if (_currentOperation == Operations.AddingRectangle)
            {
                _currentOperation = Operations.None;
            }
            else
            {
                _currentOperation = Operations.AddingRectangle;
            }
        }
        
        private void update()
        {
            canvas.Children.Clear();
            foreach (var fig in figures)
            {
                canvas.Children.Add(fig.GetFigure());
            }            
        }
        private void onFiguresChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            update();
            undoRedoManager.saveState(figures);
        }
        public ObservableCollection<IDrawable> figures { get; set; }
        public ObservableCollection<UIElement> informationFigures { get; set; }
        private UndoRedoManager undoRedoManager = new UndoRedoManager();

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string p)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(p));
            }
        }

        string savePath;

        string _canvasPosStr;
        public string CanvasPosStr {
            get { return _canvasPosStr; }
            set { _canvasPosStr = value; NotifyPropertyChanged("CanvasPosStr"); }
        }

        string _canvasSizeStr;
        public string CanvasSizeStr
        {
            get { return _canvasSizeStr; }
            set { _canvasSizeStr = value; NotifyPropertyChanged("CanvasSizeStr"); }
        }

        Point _startPoint;
        bool isFirstPoint = true;
        private void onCanvasMouseMove(object sender, MouseEventArgs e)
        {
            CanvasPosStr = "pos: " + (int)e.GetPosition(canvas).X + "," + (int)e.GetPosition(canvas).Y;
            Point point = e.GetPosition(canvas);
            switch (_currentOperation)
            {
                case Operations.AddingRectangle:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        DrawShape(point);
                    }
                    break;
                case Operations.AddingEllipse:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        DrawShape(point);
                    }
                    break;
                case Operations.AddingLine:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        DrawLine(point);
                    }
                    break;
                case Operations.PaintPencil:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        figures.Last().Move(point);
                    }
                    break;
                case Operations.PaintBrush:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        figures.Last().Move(point);
                    }
                    break;
                case Operations.Erase:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        figures.Last().Move(point);
                    }
                    break;
                case Operations.DragCanvas:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        canvas.Margin = new Thickness(canvas.Margin.Left + point.X - _startPoint.X,
                                                       canvas.Margin.Top + point.Y - _startPoint.Y,
                                                       0, 0);
                    }
                    break;
                default: break;
            }
        }

        private void DrawLine(Point point)
        {
            figures.Last().Resize(new Point(point.X, point.Y));
        }
        private void DrawShape(Point point)
        {
            if (figures.Count > 0)
            {
                if (point.X < _startPoint.X && point.Y < _startPoint.Y)
                {
                    figures.Last().Move(point);
                    figures.Last().Resize(new Point(_startPoint.X - point.X, _startPoint.Y - point.Y));
                }
                else
                {
                    if (point.X < _startPoint.X)
                    {
                        figures.Last().Move(new Point(point.X, _startPoint.Y));
                        figures.Last().Resize(new Point(_startPoint.X - point.X, point.Y - _startPoint.Y));
                    }
                    else
                    {
                        if (point.Y < _startPoint.Y)
                        {
                            figures.Last().Move(new Point(_startPoint.X, point.Y));
                            figures.Last().Resize(new Point(point.X - _startPoint.X, _startPoint.Y - point.Y));
                        }
                        else
                        {
                            figures.Last().Move(new Point(_startPoint.X, _startPoint.Y));
                            figures.Last().Resize(new Point(point.X - _startPoint.X, point.Y - _startPoint.Y));
                        }
                    }
                }
            }
        }
        private void onMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _startPoint = e.GetPosition(canvas);
            IDrawable fig = null;
            switch (_currentOperation)
            {
                case Operations.AddingRectangle:
                    fig = new MyRectangle(_startPoint);
                    break;
                case Operations.AddingEllipse:
                    fig = new MyEllipse(_startPoint);
                    break;
                case Operations.AddingLine:
                    fig = new MyLine(_startPoint, _startPoint);
                    break;
                case Operations.AddingPolygon:
                    if(isFirstPoint)
                    {
                        fig = new MyPolygon(_startPoint);
                        isFirstPoint = false;
                    }
                    else
                    {
                        (figures.Last() as MyPolygon).AddPoint(_startPoint);
                        fig = null;
                    }
                    break;
                case Operations.PaintPencil:
                    fig = new MyBrush(new MyEllipse(_startPoint), 1, canvas.RenderSize);
                    
                    break;
                case Operations.PaintBrush:
                    fig = new MyBrush(new MyEllipse(_startPoint), 10, canvas.RenderSize);
                    break;
                case Operations.Erase:
                    fig = new MyBrush(new MyEllipse(_startPoint), 10, canvas.RenderSize);
                    fig.SetBorderColor(secondColor);
                    fig.SetThickness(thickness);
                    figures.Add(fig);
                    return;
                case Operations.Fill:
                    fig = new MyRectangle(new Point(0,0), canvas.RenderSize);
                    fig.SetThickness(0);
                    fig.Fill(firstColor);
                    fig.GetFigure().MouseLeftButtonDown += figuresMouseLeftButtonDown;
                    figures.Insert(0, fig);
                    return;;
                default: break;
            }
            if(fig != null)
            {
                fig.SetBorderColor(firstColor);
                fig.SetThickness(thickness);
                fig.Fill(fillColor);
                if (!(fig is MyLine) && !(fig is MyBrush))
                {
                    fig.GetFigure().MouseLeftButtonDown += figuresMouseLeftButtonDown;
                }
                figures.Add(fig);
            }
                
        }

        private void figuresMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_currentOperation == Operations.Fill)
            {
                e.Handled = true;
                (sender as Shape).Fill = firstColor;
            }
                
        }

        private void onCanvasRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(_currentOperation == Operations.AddingPolygon)
            {
                isFirstPoint = true;
            }
        }

        private void onMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            switch(_currentOperation)
            {
                case Operations.AddingRectangle:
                    break;
                default: break;
            }
        }

        private void onZoomSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double value = zoomSlider.Value;
            canvas.LayoutTransform = new ScaleTransform(value / 100.0, value / 100.0);
        }

        private void onCanvasSizeChanged(object sender, SizeChangedEventArgs e)
        {
            CanvasSizeStr = "size: " + canvas.RenderSize;
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            figures.Clear();
            canvas.Children.Clear();
            savePath = null;
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.InitialDirectory = @"C:\";
            openDialog.Multiselect = false;
            openDialog.Filter = "Image files (*.jpg)|*.jpg|Image files (*.png)|*.png|All Files (*.*)|*.*";
            openDialog.ShowDialog();
            string nameOpenFile = openDialog.FileName;
            Image image = new Image();
            image.Source = new BitmapImage(new Uri(nameOpenFile));
            canvas.Children.Clear();
            canvas.Children.Add(image);
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Bitmap files (*.bmp)|*.bmp|Image files (*.png)|*.png";

            if ((bool)saveDialog.ShowDialog(this))
            {
                try
                {
                    using (FileStream file = new FileStream(saveDialog.FileName,
                                            FileMode.Create, FileAccess.Write))
                    {
                        savePath = saveDialog.FileName;
                        int margLeft = int.Parse(this.canvas.Margin.Left.ToString());
                        int margTop = int.Parse(this.canvas.Margin.Top.ToString());
                        RenderTargetBitmap rtb = new RenderTargetBitmap((int)this.canvas.ActualWidth + margLeft,
                                        (int)this.canvas.ActualHeight + margTop, 0, 0, PixelFormats.Default);
                        rtb.Render(this.canvas);
                        var crop = new CroppedBitmap(rtb, new Int32Rect((int)margLeft, (int)margTop, (int)canvas.ActualWidth, (int)canvas.ActualHeight));

                        BitmapEncoder encoder = new PngBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(crop));
                        encoder.Save(file);
                        file.Close();
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, Title);
                }

            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (savePath != null)
            {
                try
                {
                    using (FileStream file = new FileStream(savePath,
                                            FileMode.Create, FileAccess.Write))
                    {
                        int margLeft = int.Parse(this.canvas.Margin.Left.ToString());
                        int margTop = int.Parse(this.canvas.Margin.Top.ToString());
                        RenderTargetBitmap rtb = new RenderTargetBitmap((int)this.canvas.ActualWidth + margLeft,
                                        (int)this.canvas.ActualHeight + margTop, 0, 0, PixelFormats.Default);
                        rtb.Render(this.canvas);
                        var crop = new CroppedBitmap(rtb, new Int32Rect((int)margLeft, (int)margTop, (int)canvas.ActualWidth, (int)canvas.ActualHeight));

                        BitmapEncoder encoder = new PngBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(crop));
                        encoder.Save(file);
                        file.Close();
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, Title);
                }
            }
            else
            {
                this.SaveAs_Click(sender, e);
            }
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<IDrawable> col = new ObservableCollection<IDrawable>();
            if(undoRedoManager.undo(ref col))
            {
                figures = col;
                figures.CollectionChanged += onFiguresChanged;
                update();
            }

        }

        private void Redo_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<IDrawable> col = new ObservableCollection<IDrawable>();
            if (undoRedoManager.redo(ref col))
            {
                figures = col;
                figures.CollectionChanged += onFiguresChanged;
                update();
            }
        }

        private void Cut_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Copy_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Paste_Click(object sender, RoutedEventArgs e)
        {

        }

        private void EraseSelection_Click(object sender, RoutedEventArgs e)
        {

        }

        private void FillSelection_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Deselect_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void setForegroundColor(object sender, RoutedEventArgs e)
        {
            var colorDialog = new ColorDialog();
            if(colorDialog.ShowDialog() == true)
            {
                firstColor = colorDialog.colorPicker.SelectedColor;
                frontBtn.Background = firstColor;
            }
        }

        private void setBackgroundColor(object sender, RoutedEventArgs e)
        {
            var colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == true)
            {
                secondColor = colorDialog.colorPicker.SelectedColor;
                backBtn.Background = secondColor;
            }
        }

        private void lineBtnClicked(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            lineButton.IsChecked = true;
            _currentOperation = Operations.AddingLine;
        }

        private void dragBtnClicked(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            dragButton.IsChecked = true;
            _currentOperation = Operations.DragCanvas;
            Mouse.OverrideCursor = Cursors.Hand;
        }

        private void fillBtnClicked(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            fillButton.IsChecked = true;
            _currentOperation = Operations.Fill;
        }

        private void eraseBtnClicked(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            eraseButton.IsChecked = true;
            _currentOperation = Operations.Erase;
            thicknessComb.SelectedIndex = 9;
        }

        private void brushBtnClicked(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            brushButton.IsChecked = true;
            _currentOperation = Operations.PaintBrush;
            thicknessComb.SelectedIndex = 9;
        }

        private void pencilBtnClicked(object sender, RoutedEventArgs e)
        {
            UncheckAllButtons();
            pencilButton.IsChecked = true;
            _currentOperation = Operations.PaintPencil;
            thicknessComb.SelectedIndex = 0;
        }

        private void thicknessCombSelChanged(object sender, SelectionChangedEventArgs e)
        {
            thickness = double.Parse(((sender as ComboBox).SelectedItem as ComboBoxItem).Content as string);
        }

        private void fillColorClicked(object sender, RoutedEventArgs e)
        {
            var colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == true)
            {
                fillColor = colorDialog.colorPicker.SelectedColor;
                fillColorButton.Background = fillColor;
            }
        }

        private void canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            double zoom = e.Delta > 0 ? 5 : -5;

            zoomSlider.Value += zoom; 
        }

    }
}
