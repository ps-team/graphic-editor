﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Graphic_Editor.UI
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    [ProvideToolboxControl()]
    public partial class ToolBox : UserControl
    {
        public ToolBox()
        {
            InitializeComponent();
        }

        public void AddButtonToStack(ToggleButton b)
        {
            stack.Children.Add(b);
        }
       
        public string ButtonIcoSource
        {
            get { return (string)GetValue(ButtonIcoSourceProperty); }
            set { SetValue(ButtonIcoSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ButtonIcoSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonIcoSourceProperty =
            DependencyProperty.Register("ButtonIcoSource", typeof(string), typeof(ToolBox), new PropertyMetadata(""));

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (stack.Visibility == Visibility.Hidden)
                stack.Visibility = Visibility.Visible;
            else
                stack.Visibility = Visibility.Hidden;
        }

        private void button1_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            stack.Width = toolBox.Width - button1.Width;
        }

        public void hide()
        {
            stack.Visibility = Visibility.Hidden;
        }
    }
}
