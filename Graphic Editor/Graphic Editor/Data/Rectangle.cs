﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Graphic_Editor
{
    public class MyRectangle : IDrawable
    {
        public MyRectangle(Point point, Size size = new Size())
        {
            figure = new Rectangle();
            Move(point);
            Resize(new Point(size.Width, size.Height));
            SetBorderColor(Brushes.Black);
            (figure as Rectangle).StrokeThickness = 1;
        }
        public override void SetBorderColor(Brush color)
        {
            (figure as Rectangle).Stroke = color;
        }
        public override void Move(Point point)
        {
            Canvas.SetLeft(figure, point.X);
            Canvas.SetTop(figure, point.Y);
        }
        public override void Resize(Point size)
        {
            if (size != new Point())
            {
                (figure as Rectangle).Width = size.X;
                (figure as Rectangle).Height = size.Y;
            }
        }
        public override void SetThickness(double thickness)
        {
            (figure as Rectangle).StrokeThickness = thickness;
        }

        public override void Fill(Brush color)
        {
            (figure as Rectangle).Fill = color;
        }
        public override Point GetPosition()
        {
            return new Point(Canvas.GetLeft(figure as Rectangle), Canvas.GetTop(figure as Rectangle));
        }
    }
}
