﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Graphic_Editor
{
    public class MyLine : IDrawable
    {
        public MyLine(Point first, Point second)
        {
            figure = new Line();
            (figure as Line).X1 = first.X;
            (figure as Line).Y1 = first.Y;
            (figure as Line).X2 = second.X;
            (figure as Line).Y2 = second.Y;
            SetBorderColor(Brushes.Black);
            SetThickness(1);
        }

        public override void SetBorderColor(Brush color)
        {
            (figure as Line).Stroke = color;
        }
        public override void SetThickness(double thickness)
        {
            (figure as Line).StrokeThickness = thickness;
        }
        public override void Move(Point point)
        {
            (figure as Line).X1 = point.X;
            (figure as Line).Y1 = point.Y;
        }
        public override void Resize(Point size)
        {
            (figure as Line).X2 = size.X;
            (figure as Line).Y2 = size.Y;
        }

        public override void Fill(Brush color)
        {
            //nothing
        }
        public override Point GetPosition()
        {
            return new Point((figure as Line).X1, (figure as Line).Y1);
        }
    }
}