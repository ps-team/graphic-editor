﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Graphic_Editor
{
    public class MyPolygon: IDrawable
    {
        public MyPolygon(PointCollection pointCollection)
        {
            figure = new Polygon();
            (figure as Polygon).Points = pointCollection;
            SetBorderColor(Brushes.Black);
            (figure as Polygon).StrokeThickness = 1;
        }
        public MyPolygon(Point point)
        {
            figure = new Polygon();
            (figure as Polygon).Points.Add(point);
            (figure as Polygon).Stroke = Brushes.Black;
            (figure as Polygon).StrokeThickness = 1;
        }
        public override void SetBorderColor(Brush color)
        {
            (figure as Polygon).Stroke = color;
        }
        public void AddPoint(Point point)
        {
            (figure as Polygon).Points.Add(point);
        }
        public override void Move(Point point)
        {

        }
        public override void Resize(Point size)
        {

        }
        public Point GetLastPoint()
        {
            return (figure as Polygon).Points.Last();
        }
        public override void SetThickness(double thickness)
        {
            (figure as Polygon).StrokeThickness = thickness;
        }
        public override void Fill(Brush color)
        {
            (figure as Polygon).Fill = color;
        }
        public override Point GetPosition()
        {
            return (figure as Polygon).Points.First();
        }
    }
}
