﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Graphic_Editor
{
    public class MyEllipse: IDrawable
    {
        public MyEllipse(Point point, Size size = new Size())
        {
            figure = new Ellipse();
            Move(point);
            Resize(new Point(size.Width, size.Height));
            SetBorderColor(Brushes.Black);
            (figure as Ellipse).StrokeThickness = 1;
        }
        public override void SetBorderColor(Brush color)
        {
            (figure as Ellipse).Stroke = color;
        }
        public override void Move(Point point)
        {
            Canvas.SetLeft(figure, point.X);
            Canvas.SetTop(figure, point.Y);
        }
        public override void Resize(Point size)
        {
            if (size != new Point())
            {
                (figure as Ellipse).Width = size.X;
                (figure as Ellipse).Height = size.Y;
            }
        }
        public override void SetThickness(double thickness)
        {
            (figure as Ellipse).StrokeThickness = thickness;
        }

        public override void Fill(Brush color)
        {
            (figure as Ellipse).Fill = color;
        }
        public override Point GetPosition()
        {
            return new Point(Canvas.GetLeft(figure as Ellipse), Canvas.GetTop(figure as Ellipse));
        }
    }
}
