﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Markup;

namespace Graphic_Editor
{
    public static class ExtensionMethods
    {
        public static T XamlClone<T>(this T original) where T : class
        {
            if (original == null)
                return null;

            object clone;
            using (var stream = new MemoryStream())
            {
                XamlWriter.Save(original, stream);
                stream.Seek(0, SeekOrigin.Begin);
                clone = XamlReader.Load(stream);
            }

            if (clone is T)
                return (T)clone;
            else
                return null;
        }
    }
    public class MyBrush : IDrawable
    {
        IDrawable shape;
        public MyBrush(IDrawable fig, double thickness, Size canvasSize)
        {
            shape = fig;
            shape.SetThickness(0);
            shape.Fill(Brushes.Black);
            shape.Resize(new Point(thickness, thickness));
            figure = new Canvas() { Width = canvasSize.Width, Height = canvasSize.Height, Background = Brushes.Transparent };
        }
        public override void Move(Point point)
        {
            Point oldP = shape.GetPosition();
            double dx = (point.X - oldP.X) / 40;
            double dy = (point.Y - oldP.Y) / 40;
            for (int i = 1; i < 40; i++)
            {
                shape.Move(new Point(oldP.X + dx*i, oldP.Y + dy*i));
                (figure as Canvas).Children.Add(ExtensionMethods.XamlClone(shape.GetFigure()));
            }
            shape.Move(point);
            (figure as Canvas).Children.Add(ExtensionMethods.XamlClone(shape.GetFigure()));
        }

        public override void Resize(System.Windows.Point size)
        {
            //nothing
        }

        public override void SetBorderColor(Brush color)
        {
            shape.Fill(color);
        }

        public override void SetThickness(double thickness)
        {
            shape.Resize(new Point(thickness, thickness));
        }

        public override void Fill(Brush color)
        {
            //nothing
        }
        public override Point GetPosition()
        {
            return new Point();
        }
    }
}
