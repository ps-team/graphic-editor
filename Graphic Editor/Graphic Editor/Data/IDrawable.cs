﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Graphic_Editor
{
    public abstract class IDrawable
    {
        protected FrameworkElement figure;
        public abstract void Move(Point point);
        public abstract void Resize(Point size);
        public abstract void SetBorderColor(Brush color);
        public abstract void SetThickness(double thickness);
        public abstract void Fill(Brush color);
        public abstract Point GetPosition();

        public UIElement GetFigure()
        {
            return figure;
        }
        public IDrawable Clone()
        {
            return (IDrawable)MemberwiseClone();
        }
        private Ellipse GetInformationEllipse(Point point)
        {
            Ellipse ellipse = new Ellipse();
            Canvas.SetLeft(ellipse, point.X);
            Canvas.SetTop(ellipse, point.Y);
            ellipse.Width = 5;
            ellipse.Height = 5;
            ellipse.Stroke = Brushes.Black;
            return ellipse;
        }
        public Canvas GetInformationFigure()
        {
            Canvas canvas = new Canvas() { Background = Brushes.Transparent, Width = figure.Width, Height = figure.Height };
            canvas.Children.Add(GetInformationEllipse(new Point(0, 0)));
            canvas.Children.Add(GetInformationEllipse(new Point(0, canvas.Width / 2)));
            canvas.Children.Add(GetInformationEllipse(new Point(0, canvas.Width)));
            canvas.Children.Add(GetInformationEllipse(new Point(canvas.Height / 2, 0)));
            canvas.Children.Add(GetInformationEllipse(new Point(canvas.Height / 2, canvas.Width)));
            canvas.Children.Add(GetInformationEllipse(new Point(canvas.Height, 0)));
            canvas.Children.Add(GetInformationEllipse(new Point(canvas.Height, canvas.Width / 2)));
            canvas.Children.Add(GetInformationEllipse(new Point(canvas.Height, canvas.Width)));
            return canvas;
        }
        
    }
}
